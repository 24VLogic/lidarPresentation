import matplotlib.pyplot as plt
import numpy as np
from matplotlib.patches import Rectangle
from matplotlib.patches import Circle
from matplotlib.patches import Polygon
import matplotlib.animation as animation
from matplotlib.animation import FuncAnimation

def setup_figure():
    reflectedLightFigure = plt.figure(figsize=(6,3))
    ax1 = reflectedLightFigure.add_subplot(111)
    ax1.set_xlim(0,6)
    ax1.set_ylim(-1.5,1.5)
    ax1.set_axis_off()
    return reflectedLightFigure, ax1

def setup_emitter():
    emitterHeight = 0.125
    emitterPosition = [
        1.0,
        0-emitterHeight/2.0
        ]
    emitter = Rectangle(
        emitterPosition,
        height = emitterHeight,
        width = 0.25
        )
    ax1.add_patch(emitter)
    return emitter

def setup_detector():
    detectorHeight = 0.125
    detectorPosition = [
        1.0,
        -0.2-detectorHeight/2.0
        ]
    detector = Rectangle(
        detectorPosition,
        height = detectorHeight,
        width = 0.25
        )
    ax1.add_patch(detector)
    return detector

def setup_obj():
    objPosition = [
        5.,
        0.
        ]
    obj = Circle(
        objPosition,
        radius = 0.5
        )
    ax1.add_patch(obj)
    return obj

def setup_target():
    targetPosition = np.copy(obj.center)
    targetPosition[0] = targetPosition[0]-0.5
    target = Circle(
        targetPosition,
        radius = 0.125,
        color = 'r',
        visible = False,
        alpha = 0.8
        )
    ax1.add_patch(target)

    return target

'''
emissionPathXs = np.linspace(emitterPosition[0]+emitter.get_width(),target.center[0],10)
emissionPathYs = np.linspace(emitterPosition[1]+emitter.get_height()/2,target.center[1]+target.radius,10)
negativeEmissionPathYs = -emissionPathYs
'''

class emissionPath():
    def __init__(self):
        self.x = np.hstack(
            (
            np.linspace(emitter.xy[0]+emitter.get_width(),target.center[0],10),
            np.linspace(target.center[0],emitter.xy[0]+emitter.get_width(),10)
            )
            )

        self.y = np.hstack(
            (
            np.linspace(0,target.center[1]+target.radius,10),
            np.linspace(0,target.center[1]+target.radius,10)+target.radius,
            )
            )
def initReflection():
    patch1 = ax1.add_patch(polygon)
    patch2 = ax1.add_patch(returnPolygon)
    return patch1,

def animateReflection(*args):
    i = args[0]
    xArray = path.x[0:i+1]
    yArray = path.y[0:i+1]
    verts = np.array([[xArray[0],xArray[-1],xArray[-1]],[yArray[0],yArray[-1],-yArray[-1]]]).T
    if i >= len(path.x)/2-1:
        target.set_visible(True)
        verts = np.array([[xArray[0],path.x[9],path.x[9]],[yArray[0],path.y[9],-path.y[9]]]).T
        returnPolygon.set_xy(np.array([[path.x[9],xArray[-1],xArray[-1],path.x[9]],[path.y[9],yArray[-1],-yArray[-1],-path.y[9]]]).T)
        returnPolygon.set_visible(True)
        patch1 = ax1.add_patch(polygon)
        patch2 = ax1.add_patch(target)
        patch3 = ax1.add_patch(returnPolygon)
    else:
        target.set_visible(False)
        returnPolygon.set_visible(False)
        patch1 = ax1.add_patch(polygon)
        patch2 = ax1.add_patch(target)
        patch3 = ax1.add_patch(returnPolygon)
        verts = np.array([[xArray[0],xArray[-1],xArray[-1]],[yArray[0],yArray[-1],-yArray[-1]]]).T

    polygon.set_xy(verts)
    return patch1, patch2, patch3

fig, ax1 = setup_figure()
emitter = setup_emitter()
obj = setup_obj()
target = setup_target()
detector = setup_detector()
path = emissionPath()
polygon = Polygon(np.array([[0,0,0],[0,0,0]]).T, color = 'r', alpha = 0.5)
returnPolygon = Polygon(np.array([[0,0,0],[0,0,0]]).T, color = 'r', alpha = 0.5, visible = False)

fAnim = FuncAnimation(fig,animateReflection,frames = len(path.x),init_func=initReflection,interval = 100,blit= True)
fAnim.save('reflection.gif', dpi=80, writer='imagemagick')
